export default class Files {
    #URL;

    constructor(url) {
        this.#URL = url;
    }

    normal_read() {
        var txt = new XMLHttpRequest();
        txt.open('get', this.#URL, false);
        txt.send();
        return txt.responseText;
    }

    array_read() {
        var txt = new XMLHttpRequest();
        txt.open('get', this.#URL, false);
        txt.send();
        return txt.responseText.split(/\r\n|\n/);
    }

    csv_read() {
        var txt = new XMLHttpRequest();
        txt.open('get', this.#URL, false);
        txt.send();
        var arr = txt.responseText.split('\n');
        var res = [];
        for (var i = 0; i < arr.length; i++) {
            if (arr[i] == '') break;
            res[i] = arr[i].split(',');
        }
        return res;
    }
}