import Files from "../Packages/Files.js";
import Make from "./Make.js";

export default class MapData {
    #MapDatas;

    constructor(url) {
        //csvファイルから情報を取得・データ作成
        let MapdataFile = new Files(url);
        this.#MapDatas = new Make(MapdataFile.csv_read());
    }
}