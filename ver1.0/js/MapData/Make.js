export default class Make {
    #MapData;
    #LineData = new Array(0);
    #LineList = new Array(0);
    #StationData = new Array(0);

    constructor(data) {
        //csvファイルからの2次元配列
        this.#MapData = data;

        //路線情報
        this.#LineData = this.#LineDatas();

        //路線名情報
        this.#LineList = this.#LineNameList();

        //各路線ごとの駅名一覧情報
        this.#StationData = this.#StationDatas();
    }

    GetAllData() {
        let work = {
            LineData: this.GetLineData(),
            LineList: this.GetLineList(),
            StationData: this.GetStationData()
        };

        return work;
    }

    GetLineData() {
        return this.#LineData;
    }

    GetLineList() {
        return this.#LineList;
    }

    GetStationData() {
        return this.#StationData;
    }

    //路線情報作成
    #LineDatas() {
        //全体データから駅情報がある最上列のデータのみを取得
        let data = this.#MapData[0];

        //構造体を戻り値にするために枠を用意
        let result = new Array(0);

        //駅情報格納
        for (let cnt = 1; cnt < data.length; cnt++) {
            let work = {
                Name: "",
                Initial: "",
                Color: ""
            };
            let StationArray = String(data[cnt]).split("-");

            //名前、頭文字、色をそれぞれ代入
            work.Name = StationArray[0];
            work.Initial = StationArray[1];
            work.Color = StationArray[2];

            //結果用配列に情報を格納
            result.push(work);
        }

        return result;
    }

    //路線名一覧を作成
    #LineNameList() {
        let data = this.#LineData;
        let result = new Array(0);

        for (let cnt = 0; cnt < data.length; cnt++) {
            //StationDataから得た情報の中の駅名部分のみを格納
            let work = data[cnt].Name;
            result.push(work);
        }

        return result;
    }

    //路線ごとの駅名一覧を作成
    #StationDatas() {
        //路線名一覧を取得
        let List = this.#LineList;

        //結果用配列
        let result = new Array(0);

        //路線分の繰り返し
        for (let LineCnt = 0; LineCnt < List.length; LineCnt++) {
            //csvデータの左端にカウント値があるためLineCnt+1にして正常な値を設定している
            let StaList = this.#MapData[LineCnt + 1];

            //路線名を取得
            let LineName = String(List[LineCnt]);

            //1路線分の駅一覧を格納させる
            let work = new Array(0);

            //駅数分の繰り返し
            //最上列には路線情報があるためカウントが1から開始
            for (let StaCnt = 1; StaCnt < StaList.length; StaCnt++) {
                let StaName = StaList[StaCnt];

                //未定義か空文字なら処理しない
                if (StaName != undefined && StaName != "") {
                    work.push(StaName);
                }
            }

            //結果用配列に構造体の要素の1つとしてデータを格納
            result[LineName] = work;
        }

        //構造体としてデータを返す
        return result;
    }
}